#include <vector>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <PubSubClient.h>
#include <WiFiClientSecure.h>
#include <ESP8266WiFi.h>

#define ENABLE_PRINT 1
#if ENABLE_PRINT
    #define Sprintln(a) Serial.println(a)
    #define Sprint(a) Serial.print(a)
#else
    #define Sprintln(a)
    #define Sprint(a)
#endif

#define SENSOR_PIN A0
#define SENSOR_THRESHOLD 400

#define WIFI_SSID "ssid"
#define WIFI_PASSPHRASE "pass"

#define MQTT_VERSION MQTT_VERSION_3_1_1
#define MQTT_BROKER_HOST "192.168.43.209"
#define MQTT_BROKER_PORT 1883
#define MQTT_CLIENT_ID "ID123"
#define MQTT_TOPIC "electricPanel"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

std::vector<unsigned long> messageBuffer = {};
bool ledOn = false;

void setup()
{
    #if ENABLE_PRINT
        Serial.begin(9600);
        while(!Serial){}
    #endif
    
    connectWifi(true);
    timeClient.begin();

    mqttClient.setServer(MQTT_BROKER_HOST, MQTT_BROKER_PORT);
    connectMQTT();
}

void loop()
{
    int reading = analogRead(SENSOR_PIN);
    Sprintln(reading);
    Sprintln(ledOn);
    if (reading > SENSOR_THRESHOLD)
    {
        if (ledOn)
        {
            return;
        }
        ledOn = true;
    }
    else
    {
        if (SENSOR_THRESHOLD - reading < 10)   
        {
            return;
        }
        
        if (ledOn)
        {
            handleLEDFash();
        }
        ledOn = false;
    }
}

void handleLEDFash()
{
    Sprintln("LED FLASH");
    
    unsigned long currentTime = timeClient.getEpochTime();
    messageBuffer.push_back(currentTime);

    if (isWifiConnected())
    {
        yield(); // == delay() - needed to give CPU time to ESP8266 for internals
        timeClient.update();
        yield(); // might be unnecessary
        if (!mqttClient.connected())
        {
            connectMQTT();
            yield(); // might be unnecessary
        }
        mqttClient.loop();

        for (int messagesSent = 0; messagesSent < 3; messagesSent++)
        {
            if (messageBuffer.size() < 1)
            {
                break;
            }

            unsigned long messageContent = messageBuffer.front();
            char message [sizeof(messageContent)+2];
            sprintf(message, "%d", messageContent);

            if (mqttClient.publish(MQTT_TOPIC, message))
            {
                Sprintln(F("Message published"));
                messageBuffer.erase(messageBuffer.begin());
            }
            else
            {
                Sprintln(F("Failed publishing message"));
            }
        }
    }
    else
    {
        connectWifi(false);
    }
}

void connectMQTT()
{
    Sprintln(F("Attempting MQTT connection..."));
    if (mqttClient.connect(MQTT_CLIENT_ID))
    {
        Sprintln(F("Connected to MQTT broker"));
        mqttClient.publish(MQTT_CLIENT_ID, "ON");
    } 
    else
    {
        Sprint(F("failed, rc="));
        Sprint(mqttClient.state());
    }
}


void connectWifi(bool block)
{
    if (isWifiConnected())
    {
        return;
    }

    Sprintln(F("Connecting to wifi"));
    WiFi.begin(WIFI_SSID, WIFI_PASSPHRASE);

    if (block)
    {
        int count = 0;
        while (WiFi.waitForConnectResult() != WL_CONNECTED)
        {
            delay(100);
            count++;
            if (count > 10)
            {
                Sprintln(F("Connection status:"));
                Sprintln(WiFi.status());
                WiFi.disconnect();
                ESP.restart();
            }
        }
    }

    Sprintln(F("Connected to wifi"));
    Sprintln(WiFi.localIP());
}

bool isWifiConnected()
{
    return WiFi.status() == WL_CONNECTED;
}
