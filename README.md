# ElectricConsumption

Use a ESP8266 to measure electric consumption based on led flashes of the breaker box. These readings are sent over to a MQTT broker for further use.